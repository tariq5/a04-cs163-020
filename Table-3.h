//tariq khandwalla
//A04 CS163
//Tuesday May 21, 2024
//Purpose: This is the header file for the table class. It contains the class definition for the table class.


#ifndef TABLE_H
#define TABLE_H

#include <iostream>
#include <cstring>

using namespace std;

class BSTNode {
public:
    char name[100];
    char description[500];
    char details[3][100];
    int search_history;
    char extra_info[100];
    BSTNode* left;
    BSTNode* right;

    BSTNode(const char* name, const char* description, const char details[3][100], 
            int search_history = 0, const char* extra_info = "");
};

class Table {
public:
    Table();
    ~Table();

    void add(const char* name, const char* description, const char details[3][100], 
             int search_history = 0, const char* extra_info = "");
    void display_all() const;
    bool remove(const char* name);
    BSTNode* find(const char* name) const;
    void display_by_aspect(const char* aspect) const;
    int height() const;

private:
    BSTNode* root;

    // Private recursive functions
    BSTNode* add(BSTNode* node, const char* name, const char* description, 
                 const char details[3][100], int search_history, const char* extra_info);
    void display_all(BSTNode* node) const;
    BSTNode* find(BSTNode* node, const char* name) const;
    BSTNode* remove(BSTNode* node, const char* name, bool& success);
    BSTNode* min_value_node(BSTNode* node) const;
    void destroy_tree(BSTNode* node);
    void display_by_aspect(BSTNode* node, const char* aspect) const;
    int height(BSTNode* node) const;
};

#endif
